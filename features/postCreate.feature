@database
Feature: Create a post
  In order to add new information to post collection
  As a user
  I want to create a post
  Scenario: Creating a post with all required data
    Given I am authenticated as a user
    When I send a "POST" request to "/posts" with body:
    """
    {
      "author": "Chris Tankersley",
      "title": "Docker for Developers",
      "content": "Start using containers to make your development and deployment easier."
    }
    """
    Then I should receive a response that conform to the specification
    And I should receive a response with status 202

  Scenario: Creating a post without all required data
    Given I am authenticated as a user
    When I send a "POST" request to "/posts" with body:
    """
    {
      "author": "Chris Tankersley",
      "title": "Docker for Developers"
    }
    """
    Then I should receive a response that conform to the specification
    And I should receive a response with status 400
    And I should receive a response with JSON nodes "tag, target, message, details"
    And I should receive a response with value "INVALID_OR_NULL_VALUE" for JSON node "target"