@database
Feature: Get a post by id
  In order to access the complete information of the post
  As a user
  I want to get a post by id
  Scenario: Getting a post by a valid id
    Given I have an item in database with identifier equals to "1e539e7c-5b95-41cc-becf-eab1a67f4c9d"
    And I am authenticated as a user
    When I send a "GET" request to "/posts/1e539e7c-5b95-41cc-becf-eab1a67f4c9d"
    Then I should receive a response that conform to the specification
    And I should receive a response with status 200
    And I should receive a response with JSON nodes "id, author, title, content"

  Scenario: Getting a post by a invalid id
    Given I am authenticated as a user
    When I send a "GET" request to "/posts/1e539e7c"
    Then I should receive a response that conform to the specification
    And I should receive a response with status 400
    And I should receive a response with JSON nodes "tag, target, message, details"
    And I should receive a response with value "INVALID_OR_NULL_VALUE" for JSON node "target"

  Scenario: Getting a post by a non-existent id
    Given I am authenticated as a user
    When I send a "GET" request to "/posts/1e539e7c-5b95-41cc-becf-eab1a67f4c9d"
    Then I should receive a response that conform to the specification
    And I should receive a response with status 404
    And I should receive a response with JSON nodes "tag, target, message"
    And I should receive a response with value "NOT_FOUND" for JSON node "target"
