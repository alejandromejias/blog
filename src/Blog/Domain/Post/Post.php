<?php

declare(strict_types=1);

namespace App\Blog\Domain\Post;

use App\Blog\Shared\Domain\Bus\Event\DomainEventPublisher;

final class Post
{
    public function __construct(
        private PostId $id,
        private string $author,
        private string $title,
        private string $content
    ) {
    }

    public static function create(
        PostId $id,
        string $author,
        string $title,
        string $content
    ): self {
        $instance = new self($id, $author, $title, $content);
        DomainEventPublisher::instance()->publish(new PostCreatedEvent($instance->toArray()));
        return $instance;
    }

    public function id(): PostId
    {
        return $this->id;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->__toString(),
            'author' => $this->author,
            'title' => $this->title,
            'content' => $this->content
        ];
    }
}
