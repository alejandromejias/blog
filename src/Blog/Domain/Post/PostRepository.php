<?php

declare(strict_types=1);

namespace App\Blog\Domain\Post;

interface PostRepository
{
    public function nextIdentity(): PostId;
    public function save(Post $post): void;
    public function ofId(PostId $id): ?Post;
}