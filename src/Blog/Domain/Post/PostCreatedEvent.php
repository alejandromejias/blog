<?php

declare(strict_types=1);

namespace App\Blog\Domain\Post;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;

final class PostCreatedEvent extends DomainEvent
{
    public static function name(): string
    {
        return 'post.created';
    }
}