<?php

declare(strict_types=1);

namespace App\Blog\Domain\Post;

use App\Blog\Shared\Domain\Uuid\Uuid;

final class PostId extends Uuid
{
}
