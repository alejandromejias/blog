<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\UnauthorizedException;

class UserUnauthorizedException extends UnauthorizedException
{
    public static function invalidCredentials(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::UNAUTHORIZED_CODE,
            'user',
            'Invalid authentication credentials.'
        );
    }

    public static function invalidJWT(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::UNAUTHORIZED_CODE,
            'token',
            'Invalid JWT token.'
        );
    }

    public static function notFoundJWT(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::UNAUTHORIZED_CODE,
            'token',
            'JWT token not found.'
        );
    }

    public static function expiredJWT(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::UNAUTHORIZED_CODE,
            'token',
            'JWT token expired.'
        );
    }
}