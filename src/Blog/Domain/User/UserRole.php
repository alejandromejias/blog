<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

enum UserRole: string
{
    case ROLE_USER = 'ROLE_USER';
    case ROLE_ADMIN = 'ROLE_ADMIN';

    public function toString(): string
    {
        return $this->name;
    }
}