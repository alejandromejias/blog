<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;

class UserCreatedEvent extends DomainEvent
{
    public static function name(): string
    {
        return 'user.created';
    }
}