<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

interface UserRepository
{
    public function nextIdentity(): UserId;
    public function save(User $user): void;
    public function ofId(UserId $id): User;
}