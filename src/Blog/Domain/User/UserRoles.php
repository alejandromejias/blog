<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

use App\Blog\Shared\Domain\Collection\Collection;

class UserRoles extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof UserRole;
    }

    public static function createFromPrimitives(array $roles): self
    {
        $userRoles = array_map(function(string $userRole) {
            return UserRole::from($userRole);
        }, $roles);
        return new self($userRoles);
    }
}