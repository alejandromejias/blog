<?php

declare(strict_types=1);

namespace App\Blog\Domain\User;

use App\Blog\Shared\Domain\Bus\Event\DomainEventPublisher;
use App\Blog\Shared\Domain\Email\Email;

class User
{
    public function __construct(
        protected UserId $id,
        protected Email $email,
        protected string $password,
        protected UserRoles $roles
    ) {
    }

    public static function create(
        UserId $id,
        Email $email,
        string $password,
        UserRoles $roles
    ): self {
        $instance = new self($id, $email, $password, $roles);
        DomainEventPublisher::instance()->publish(new UserCreatedEvent($instance->toArray()));
        return $instance;
    }

    public function id(): UserId
    {
        return $this->id;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function roles(): UserRoles
    {
        return $this->roles;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}