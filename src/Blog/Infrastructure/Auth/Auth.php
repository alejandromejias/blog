<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\Auth;

use App\Blog\Domain\User\User;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Shared\Domain\Email\Email;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Auth implements UserInterface, PasswordAuthenticatedUserInterface
{
    public function __construct(
        private UserId $id,
        private string $email,
        private string $password,
        private UserRoles $roles
    ) {
    }

    public function id(): UserId
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function roles(): UserRoles
    {
        return $this->roles;
    }

    public function getRoles(): array
    {
        return $this->roles->toArray();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
