<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\User;

use App\Blog\Domain\User\UserId;
use App\Blog\Shared\Infrastructure\Doctrine\UuidType;

class UserIdType extends UuidType
{
    protected function className(): string
    {
        return UserId::class;
    }

    protected function customName(): string
    {
        return 'user_id';
    }
}