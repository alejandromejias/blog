<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\User;

use App\Blog\Domain\User\UserRole;
use App\Blog\Domain\User\UserRoles;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ObjectType;

class UserRolesType extends ObjectType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return json_encode($value->toArray());
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return UserRoles::createFromPrimitives(json_decode($value));
    }

    public function getName(): string
    {
        return 'user_roles';
    }
}