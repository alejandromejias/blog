<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\User;

use App\Blog\Domain\User\User;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Infrastructure\Auth\Auth;
use App\Blog\Shared\Domain\Email\Email;

class MappingUserRepository
{
    public static function fromUserToAuth(User $user): Auth
    {
        return new Auth(
            $user->id(),
            $user->email()->value(),
            $user->password(),
            $user->roles()
        );
    }

    public static function fromAuthToUser(Auth $auth): User
    {
        return new User(
            $auth->id(),
            new Email($auth->email()),
            $auth->getPassword(),
            UserRoles::createFromPrimitives($auth->getRoles())
        );
    }
}