<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\User;

use App\Blog\Domain\User\User;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRepository;
use App\Blog\Infrastructure\Auth\Auth;
use App\Blog\Shared\Domain\Exception\NotFoundException;
use App\Blog\Shared\Infrastructure\Doctrine\DoctrineRepository;

class DoctrineUserRepository extends DoctrineRepository implements UserRepository
{
    public function nextIdentity(): UserId
    {
        return UserId::create();
    }

    public function save(User $user): void
    {
        $this->persist(MappingUserRepository::fromUserToAuth($user));
    }

    public function ofId(UserId $id): User
    {
        $auth = $this->repository(Auth::class)->find($id);
        if (!$auth) {
            throw new NotFoundException();
        }
        return MappingUserRepository::fromAuthToUser($auth);
    }
}