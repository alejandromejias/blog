<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\User;

use App\Blog\Domain\User\UserRoles;
use App\Blog\Shared\Domain\Email\Email;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserJWT
{
    private array $decodedJWT;

    public function __construct(
        private TokenStorageInterface $tokenStorage,
        private JWTTokenManagerInterface $jwtManager
    ) {
        $this->decodedJWT = $this->decodedJWT();
    }

    private function decodedJWT(): array
    {
        return (array)$this->jwtManager->decode($this->tokenStorage->getToken());
    }

    public function email(): Email
    {
        return new Email($this->decodedJWT['email']);
    }

    public function roles(): UserRoles
    {
        return UserRoles::createFromPrimitives($this->decodedJWT['roles']);
    }
}