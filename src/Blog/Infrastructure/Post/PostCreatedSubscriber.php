<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\Post;

use App\Blog\Domain\Post\PostCreatedEvent;
use App\Blog\Shared\Domain\Bus\Event\DomainEvent;
use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;
use Psr\Log\LoggerInterface;

class PostCreatedSubscriber implements DomainEventSubscriber
{
    public function __construct(
        private LoggerInterface $logger
    ) {}

    public function handle(DomainEvent $event): void
    {
        $this->logger->info($event::name(), $event->attributes());
    }

    public static function isSubscribedTo(DomainEvent $event): bool
    {
        return $event instanceof PostCreatedEvent;
    }

    public static function subscribedTo(): array
    {
        return [PostCreatedEvent::class];
    }
}