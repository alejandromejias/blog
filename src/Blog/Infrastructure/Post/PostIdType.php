<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\Post;

use App\Blog\Domain\Post\PostId;
use App\Blog\Shared\Infrastructure\Doctrine\UuidType;

class PostIdType extends UuidType
{
    protected function className(): string
    {
        return PostId::class;
    }

    protected function customName(): string
    {
        return 'post_id';
    }
}