<?php

declare(strict_types=1);

namespace App\Blog\Infrastructure\Post;

use App\Blog\Domain\Post\Post;
use App\Blog\Domain\Post\PostId;
use App\Blog\Domain\Post\PostRepository;
use App\Blog\Shared\Infrastructure\Doctrine\DoctrineRepository;

final class DoctrinePostRepository extends DoctrineRepository implements PostRepository
{
    public function nextIdentity(): PostId
    {
        return PostId::create();
    }

    public function save(Post $post): void
    {
        $this->persist($post);
    }

    public function ofId(PostId $id): ?Post
    {
        return $this->repository(Post::class)->find($id);
    }
}