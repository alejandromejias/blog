<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\Command;

use App\Blog\Shared\Domain\Bus\Command\Command;

final class PostCreateCommand implements Command
{
    public function __construct(
        private string $author,
        private string $title,
        private string $content
    ) {}

    public function author(): string
    {
        return $this->author;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function content(): string
    {
        return $this->content;
    }
}