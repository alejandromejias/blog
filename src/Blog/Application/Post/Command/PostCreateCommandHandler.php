<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\Command;

use App\Blog\Application\Post\UseCase\PostCreateUseCase;
use App\Blog\Shared\Domain\Bus\Command\CommandHandler;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class PostCreateCommandHandler implements CommandHandler
{
    public function __construct(
        private PostCreateUseCase $postCreateUseCase
    ) {
    }

    public function __invoke(PostCreateCommand $command): void
    {
        ($this->postCreateUseCase)(
            $command->author(),
            $command->title(),
            $command->content()
        );
    }
}