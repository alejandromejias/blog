<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\Query;

use App\Blog\Shared\Domain\Bus\Query\Query;

final class PostOfIdQuery implements Query
{
    public function __construct(
        private string $id
    ) {}

    public function id(): string
    {
        return $this->id;
    }
}