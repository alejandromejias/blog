<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\Query;

use App\Blog\Application\Post\UseCase\PostOfIdUseCase;
use App\Blog\Domain\Post\PostId;
use App\Blog\Shared\Application\Response\Response;
use App\Blog\Shared\Domain\Bus\Query\QueryHandler;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class PostOfIdQueryHandler implements QueryHandler
{
    public function __construct(
        private PostOfIdUseCase $postOfIdUseCase
    ) {}

    public function __invoke(PostOfIdQuery $query): Response
    {
        $id = new PostId($query->id());
        return ($this->postOfIdUseCase)($id);
    }
}