<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\UseCase;

use App\Blog\Application\Post\Response\PostOfIdResponse;
use App\Blog\Domain\Post\PostId;
use App\Blog\Domain\Post\PostRepository;
use App\Blog\Shared\Domain\Exception\NotFoundException;

class PostOfIdUseCase
{
    public function __construct(
        private PostRepository $postRepository
    ) {}

    public function __invoke(PostId $id): PostOfIdResponse
    {
        $post = $this->postRepository->ofId($id);
        if (empty($post)) {
            throw new NotFoundException();
        }
        return new PostOfIdResponse($post);
    }
}