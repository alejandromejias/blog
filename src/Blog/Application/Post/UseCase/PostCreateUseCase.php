<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\UseCase;

use App\Blog\Domain\Post\Post;
use App\Blog\Domain\Post\PostRepository;

final class PostCreateUseCase
{
    public function __construct(
        private PostRepository $postRepository
    ) {}

    public function __invoke(string $author, string $title, string $content): void
    {
        $post = Post::create(
            $this->postRepository->nextIdentity(),
            $author,
            $title,
            $content
        );
        $this->postRepository->save($post);
    }
}