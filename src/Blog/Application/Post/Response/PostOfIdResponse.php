<?php

declare(strict_types=1);

namespace App\Blog\Application\Post\Response;

use App\Blog\Domain\Post\Post;
use App\Blog\Shared\Application\Response\Response;

final class PostOfIdResponse implements Response
{
    public function __construct(
        private Post $post
    ) {
    }

    public function toArray(): array
    {
        return $this->post->toArray();
    }
}