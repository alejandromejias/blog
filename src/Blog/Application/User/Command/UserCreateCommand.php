<?php

declare(strict_types=1);

namespace App\Blog\Application\User\Command;

use App\Blog\Shared\Domain\Bus\Command\Command;

final class UserCreateCommand implements Command
{
    public function __construct(
        private string $id,
        private string $email,
        private string $password,
        private array $roles
    ) {}

    public function id(): string
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function roles(): array
    {
        return $this->roles;
    }
}