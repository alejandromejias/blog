<?php

declare(strict_types=1);

namespace App\Blog\Application\User\Command;

use App\Blog\Application\User\UseCase\UserCreateUseCase;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Shared\Domain\Bus\Command\CommandHandler;
use App\Blog\Shared\Domain\Email\Email;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class UserCreateCommandHandler implements CommandHandler
{
    public function __construct(
        private UserCreateUseCase $userCreateUseCase
    ) {
    }

    public function __invoke(UserCreateCommand $command): void
    {
        ($this->userCreateUseCase)(
            new UserId($command->id()),
            new Email($command->email()),
            $command->password(),
            UserRoles::createFromPrimitives($command->roles())
        );
    }
}