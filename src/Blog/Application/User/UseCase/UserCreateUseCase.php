<?php

declare(strict_types=1);

namespace App\Blog\Application\User\UseCase;

use App\Blog\Domain\User\User;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRepository;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Shared\Domain\Email\Email;

class UserCreateUseCase
{
    public function __construct(
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(
        UserId $id,
        Email $email,
        string $password,
        UserRoles $roles
    ): void {
        $user = User::create(
            $id,
            $email,
            $password,
            $roles
        );
        $this->userRepository->save($user);
    }
}