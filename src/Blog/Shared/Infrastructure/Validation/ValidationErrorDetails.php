<?php

namespace App\Blog\Shared\Infrastructure\Validation;

use App\Blog\Shared\Domain\Collection\Collection;

class ValidationErrorDetails extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof ValidationErrorDetail;
    }
}