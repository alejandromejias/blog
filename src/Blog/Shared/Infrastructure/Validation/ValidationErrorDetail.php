<?php

namespace App\Blog\Shared\Infrastructure\Validation;

use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\ExceptionDetailMessage;

class ValidationErrorDetail
{
    public function __construct(
        private string $code,
        private string $target,
        private string $message
    ) {}

    public function code(): string
    {
        return $this->code;
    }

    public function target(): string
    {
        return $this->target;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public static function createFromNullValue(string $target): self
    {
        return new self(
            ExceptionDetailCode::NULL_VALUE_CODE,
            $target,
            ExceptionDetailMessage::NULL_VALUE_MESSAGE
        );
    }

    public static function createFromInvalidValue(string $target): self
    {
        return new self(
            ExceptionDetailCode::INVALID_VALUE_CODE,
            $target,
            ExceptionDetailMessage::INVALID_VALUE_MESSAGE
        );
    }

}