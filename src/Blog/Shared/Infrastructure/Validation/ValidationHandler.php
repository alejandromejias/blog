<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Validation;

class ValidationHandler
{
    private ValidationErrorDetails $errors;

    public function __construct()
    {
        $this->errors = new ValidationErrorDetails();
    }

    public function errors(): ValidationErrorDetails
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return $this->errors->isNotEmpty();
    }

    public function handleError(ValidationErrorDetail $error): void
    {
        $this->errors->add($error);
    }
}