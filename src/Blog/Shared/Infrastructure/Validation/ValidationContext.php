<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Validation;

use App\Blog\Shared\Domain\Exception\BadRequestException;

class ValidationContext
{
    public function __construct(
        private ValidationStrategies $strategies,
        private ValidationHandler $handler
    ) {}

    public function handler(): ValidationHandler
    {
        return $this->handler;
    }

    public function addValidationStrategy(ValidationStrategy $strategy): self
    {
        $this->strategies->add($strategy);
        return $this;
    }

    public function validateOrFail(): void
    {
        foreach ($this->strategies as $strategy) {
            $this->handleStrategyErrors($strategy);
        }
        $this->checkErrors();
    }

    private function handleStrategyErrors(ValidationStrategy $strategy): void
    {
        foreach ($strategy->errors() as $error) {
            $this->handler->handleError($error);
        }
    }

    private function checkErrors(): void
    {
        if (!$this->handler()->hasErrors()) {
            return;
        }
        $exception = new BadRequestException();
        $errors = $this->handler()->errors();
        foreach ($errors as $error) {
            $exception->addDetail($error->code(), $error->target(), $error->message());
        }
        throw $exception;
    }
}