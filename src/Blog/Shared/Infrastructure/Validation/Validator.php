<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Validation;

use App\Blog\Shared\Domain\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;

abstract class Validator
{
    public function __construct(
        protected ValidationContext $context
    ) {}

    public abstract function validateOrFail(Request $request): void;
}