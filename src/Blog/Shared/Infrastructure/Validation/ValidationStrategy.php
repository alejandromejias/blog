<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Validation;

abstract class ValidationStrategy
{
    protected ValidationErrorDetails $errors;

    public function __construct()
    {
        $this->errors = new ValidationErrorDetails();
    }

    protected function addError(ValidationErrorDetail $error)
    {
        $this->errors->add($error);
    }

    public function errors(): ValidationErrorDetails
    {
        return $this->errors;
    }
}