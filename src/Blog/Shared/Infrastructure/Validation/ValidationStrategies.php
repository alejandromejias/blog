<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Validation;

use App\Blog\Shared\Domain\Collection\Collection;

class ValidationStrategies extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof ValidationStrategy;
    }
}