<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\Command;

use App\Blog\Shared\Domain\Bus\Command\Command;
use App\Blog\Shared\Domain\Bus\Command\CommandBus;
use App\Blog\Shared\Domain\Bus\Event\DomainEventBus;
use App\Blog\Shared\Domain\Bus\Event\DomainEventPublisher;
use Symfony\Component\Messenger\MessageBusInterface;

final class MessageCommandBus implements CommandBus
{
    public function __construct(
        private MessageBusInterface $commandBus,
        private DomainEventBus $eventBus
    ) {
    }

    public function dispatch(Command $command): void
    {
        $this->commandBus->dispatch($command);
        $events = DomainEventPublisher::instance()->published();
        $this->eventBus->dispatch($events);
    }
}