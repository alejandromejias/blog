<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;

final class RabbitMQConfigurator
{
    private RabbitMQExchange $exchange;
    private RabbitMQExchange $retryExchange;
    private RabbitMQExchange $deadLetterExchange;

    public function __construct(
        private RabbitMQConnection $connection,
        private RabbitMQQueueNameFormatter $queueNameFormatter
    ) {
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function __invoke(string $exchangeName, array $subscribers): void
    {
        $this->declareExchanges($exchangeName);
        $this->declareQueues($subscribers);
    }

    private function declareExchanges(string $exchangeName): void
    {
        $this->exchange = $this->declareExchange(
            RabbitMQExchangeNameFormatter::format($exchangeName)
        );
        $this->retryExchange = $this->declareExchange(
            RabbitMQExchangeNameFormatter::retry($exchangeName)
        );
        $this->deadLetterExchange = $this->declareExchange(
            RabbitMQExchangeNameFormatter::deadLetter($exchangeName)
        );
    }

    private function declareExchange(string $exchangeName): RabbitMQExchange
    {
        $declaredExchange = new RabbitMQExchange($exchangeName);
        $this->connection->declareExchange($declaredExchange);
        return $declaredExchange;
    }

    private function declareQueues(array $subscribers): void
    {
        foreach ($subscribers as $subscriber) {
            $this->queueDeclarator($subscriber);
        }
    }

    private function queueDeclarator(
        DomainEventSubscriber $subscriber
    ): void {
        $queueName = $this->queueNameFormatter->format($subscriber);
        $retryQueueName = $this->queueNameFormatter->retry($subscriber);
        $deadLetterQueueName = $this->queueNameFormatter->deadLetter($subscriber);
        $queue = $this->declareQueue($queueName);
        $retryQueue = $this->declareQueue($retryQueueName, $this->exchange->name(), $queueName, 1000);
        $deadLetterQueue = $this->declareQueue($deadLetterQueueName);
        $this->connection->bindQueue($queue, $this->exchange, $queueName);
        $this->connection->bindQueue($retryQueue, $this->retryExchange, $queueName);
        $this->connection->bindQueue($deadLetterQueue, $this->deadLetterExchange, $queueName);
        $this->bindDomainEvents($queue, $subscriber);
    }

    private function declareQueue(
        string $queueName,
        ?string $deadLetterExchange = null,
        ?string $deadLetterRoutingKey = null,
        ?int $messageTtl = null
    ): RabbitMQQueue {
        $queue = new RabbitMQQueue($queueName);
        if ($deadLetterExchange) {
            $queue->setDeadLetterExchange($deadLetterExchange);
        }
        if ($deadLetterRoutingKey) {
            $queue->setDeadLetterRoutingKey($deadLetterRoutingKey);
        }
        if ($messageTtl) {
            $queue->setMessageTtl($messageTtl);
        }
        $this->connection->declareQueue($queue);
        return $queue;
    }

    private function bindDomainEvents(RabbitMQQueue $queue, DomainEventSubscriber $subscriber): void
    {
        $subscribedDomainEvents = $subscriber::subscribedTo();
        foreach ($subscribedDomainEvents as $domainEvent) {
            $routingKey = RabbitMQTypeFormatter::format($domainEvent);
            $this->connection->bindQueue($queue, $this->exchange, $routingKey);
        }
    }
}