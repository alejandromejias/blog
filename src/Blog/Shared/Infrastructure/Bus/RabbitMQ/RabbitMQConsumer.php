<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

final class RabbitMQConsumer
{
    private const APPLICATION_HEADERS = 'application_headers';
    private const EXPIRATION = 'expiration';
    private const DELIVERY_MODE = 'delivery_mode';
    private const RETRY_COUNT = 'retry_count';
    private string $exchangeName;
    private int $maxRetries;

    public function __construct(
        private RabbitMQConnection $connection,
        private RabbitMQMessageDeserializer $messageDeserializer,
        ?string $exchangeName = null,
        ?int $maxRetries = null
    ) {
        $this->exchangeName = $exchangeName ?? 'blog';
        $this->maxRetries = $maxRetries ?? 5;
    }

    public function consume(DomainEventSubscriber $subscriber, string $queueName): void
    {
        $this->connection->consume(
            $queueName,
            $this->handle($subscriber, $queueName)
        );
    }

    private function handle(DomainEventSubscriber $subscriber, string $queueName): callable
    {
        return function (AMQPMessage $message) use ($subscriber, $queueName) {
            try {
                $event = $this->messageDeserializer->deserialize($message->getBody());
                $subscriber->handle($event);
                $message->ack();
            } catch (\Throwable $error) {
                $this->handleConsumptionError($message, $queueName);
                throw $error;
            }
        };
    }

    private function handleConsumptionError(AMQPMessage $message, string $queueName): void
    {
        $this->hasBeenRedeliveredTooMuch($message)
            ? $this->sendToDeadLetter($message, $queueName)
            : $this->sendToRetry($message, $queueName);
        $message->ack();
    }

    private function hasBeenRedeliveredTooMuch(AMQPMessage $message): bool
    {
        return $this->getRetryCountFromMessage($message) >= $this->maxRetries;
    }

    private function sendToDeadLetter(AMQPMessage $message, string $queueName): void
    {
        $message->set(self::DELIVERY_MODE, AMQPMessage::DELIVERY_MODE_NON_PERSISTENT);
        $message->set(self::EXPIRATION, 43200000);
        $this->sendMessageTo(
            RabbitMQExchangeNameFormatter::deadLetter($this->exchangeName),
            $message,
            $queueName
        );
    }

    private function sendMessageTo(string $exchangeName, AMQPMessage $message, string $queueName): void
    {
        $headers = $message->has(self::APPLICATION_HEADERS)
            ? $message->get(self::APPLICATION_HEADERS)
            : new AMQPTable();
        $retryCount = $this->getRetryCountFromMessage($message);
        $retryCount++;
        $headers->set(self::RETRY_COUNT, $retryCount, AMQPTable::T_INT_SHORT);
        $message->set(self::APPLICATION_HEADERS, $headers);
        $this->connection->publish($message, $exchangeName, $queueName);
    }

    private function getRetryCountFromMessage(AMQPMessage $message): int
    {
        $retryCount = 0;
        if ($message->has(self::APPLICATION_HEADERS)) {
            $dataHeaders = $message->get(self::APPLICATION_HEADERS)->getNativeData();
            $retryCount = $dataHeaders[self::RETRY_COUNT] ?? 0;
        }
        return $retryCount;
    }

    private function sendToRetry(AMQPMessage $message, string $queueName): void
    {
        $this->sendMessageTo(RabbitMQExchangeNameFormatter::retry($this->exchangeName), $message, $queueName);
    }
}