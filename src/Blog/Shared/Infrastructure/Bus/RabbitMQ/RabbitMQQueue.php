<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use PhpAmqpLib\Wire\AMQPTable;

final class RabbitMQQueue
{
    public function __construct(
        private string $name,
        private bool $passive = false,
        private bool $durable = true,
        private bool $exclusive = false,
        private bool $autoDelete = false,
        private bool $nowait = false,
        private ?AMQPTable $arguments = null
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function passive(): bool
    {
        return $this->passive;
    }

    public function durable(): bool
    {
        return $this->durable;
    }

    public function exclusive(): bool
    {
        return $this->exclusive;
    }

    public function autoDelete(): bool
    {
        return $this->autoDelete;
    }

    public function nowait(): bool
    {
        return $this->nowait;
    }

    public function arguments(): ?AMQPTable
    {
        return $this->arguments;
    }

    public function setDeadLetterExchange(string $deadLetterExchange): void
    {
        $this->setArgumentsIfNull();
        $this->arguments->set('x-dead-letter-exchange', $deadLetterExchange);
    }

    public function setDeadLetterRoutingKey(string $deadLetterRoutingKey): void
    {
        $this->setArgumentsIfNull();
        $this->arguments->set('x-dead-letter-routing-key', $deadLetterRoutingKey);
    }

    public function setMessageTtl(int $messageTtl): void
    {
        $this->setArgumentsIfNull();
        $this->arguments->set('x-message-ttl', $messageTtl);
    }

    private function setArgumentsIfNull(): void
    {
        if (is_null($this->arguments)) {
            $this->arguments = new AMQPTable();
        }
    }
}