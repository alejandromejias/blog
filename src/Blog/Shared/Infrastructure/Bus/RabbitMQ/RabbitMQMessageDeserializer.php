<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;
use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Blog\Shared\Infrastructure\Bus\Subscriber\Subscribers;

final class RabbitMQMessageDeserializer
{
    public function __construct(
        private Subscribers $subscribers
    ) {}

    public function deserialize(string $encodedDomainEvent): DomainEvent
    {
        $decodedDomainEvent = json_decode($encodedDomainEvent, true);
        $eventType = $decodedDomainEvent['data']['type'];
        $eventClassName = RabbitMQTypeFormatter::getEventClassNameFromType($eventType);
        $eventClass = self::getEventClassFromSubscribers($eventClassName);
        return $eventClass::fromPrimitives(
            $decodedDomainEvent['data']['id'],
            $decodedDomainEvent['data']['occurredOn'],
            $decodedDomainEvent['data']['attributes'],
        );
    }

    private function getEventClassFromSubscribers(string $eventClassName): string
    {
        foreach ($this->subscribers->all() as $subscriber) {
            $subscribedEvent = self::getEventClassFromSubscriber($eventClassName, $subscriber);
            if ($subscribedEvent) {
                return $subscribedEvent;
            }
        }
        // TODO: THROW EXCEPTION
    }

    private function getEventClassFromSubscriber(
        string $eventClassName,
        DomainEventSubscriber $subscriber
    ): ?string {
        $subscribedEvents = $subscriber::subscribedTo();
        foreach ($subscribedEvents as $subscribedEvent) {
            $subscribedEventParts = explode('\\', $subscribedEvent);
            $subscribedEventPartsCount = count($subscribedEventParts);
            --$subscribedEventPartsCount;
            $eventClassNameFromSubscriber = $subscribedEventParts[$subscribedEventPartsCount];
            if ($eventClassName === $eventClassNameFromSubscriber) {
                return $subscribedEvent;
            }
        }
        return null;
    }
}