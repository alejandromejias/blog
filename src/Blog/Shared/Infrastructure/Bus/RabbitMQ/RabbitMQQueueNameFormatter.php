<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;
use App\Blog\Shared\Infrastructure\Bus\Subscriber\Subscribers;

final class RabbitMQQueueNameFormatter
{
    public function __construct(
        private Subscribers $subscribers
    ) {}

    public function format(DomainEventSubscriber $subscriber): string
    {
        return self::queueNameFormat($subscriber);
    }

    public function retry(DomainEventSubscriber $subscriber): string
    {
        return sprintf('retry.%s', self::queueNameFormat($subscriber));
    }

    public function deadLetter(DomainEventSubscriber $subscriber): string
    {
        return sprintf('dead_letter.%s', self::queueNameFormat($subscriber));
    }

    private function queueNameFormat(DomainEventSubscriber $subscriber): string
    {
        $exchangeName = strtolower('blog');
        $subscriberClassName = self::getSubscriberClassName($subscriber);
        return sprintf('%s.%s', $exchangeName, $subscriberClassName);
    }

    private function getSubscriberClassName(DomainEventSubscriber $subscriber): string
    {
        return lcfirst((new \ReflectionClass($subscriber))->getShortName());
    }

    public function getSubscriberFromQueueName(string $queueName): DomainEventSubscriber
    {
        $queueNameParts = explode('.', $queueName);
        $queueNamePartsCount = count($queueNameParts);
        --$queueNamePartsCount;
        $queueSubscriberName = ucfirst($queueNameParts[$queueNamePartsCount]);
        return self::findSubscriberOrFail($queueSubscriberName);
    }

    private function findSubscriberOrFail(string $queueSubscriberName): DomainEventSubscriber
    {
        foreach ($this->subscribers->all() as $subscriber) {
            $subscriberClass = explode('\\', $subscriber::class);
            $subscriberClassCount = count($subscriberClass);
            --$subscriberClassCount;
            $subscriberName = $subscriberClass[$subscriberClassCount];
            if ($subscriberName === $queueSubscriberName) {
                return $subscriber;
            }
        }
        throw RabbitMQInternalServerError::subscriberNotFound($queueSubscriberName);
    }
}