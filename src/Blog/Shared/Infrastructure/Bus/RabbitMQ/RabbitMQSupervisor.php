<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEventSubscriber;

class RabbitMQSupervisor
{
    public function __construct(
        private RabbitMQQueueNameFormatter $queueNameFormatter
    ) {
    }

    public function __invoke(DomainEventSubscriber $subscriber): void
    {
        $queueName = $this->queueNameFormatter->format($subscriber);
        $fileContent = str_replace('{queue_name}', $queueName, $this->template());
        file_put_contents("/var/www/html/.deploy/dev/docker/symfony/supervisor/conf.d/${queueName}.conf", $fileContent);
    }

    private function template(): string
    {
        return <<<EOT
        [program: {queue_name}]
        command = /var/www/html/bin/console rabbitmq:consume {queue_name}
        process_name = %(program_name)s_%(process_num)02d
        numprocs = 1
        startsecs = 1
        startretries = 10
        exitcodes = 2
        autostart=true
        autorestart=true
        EOT;

    }
}