<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;
use App\Blog\Shared\Domain\Bus\Event\DomainEventBus;
use App\Blog\Shared\Domain\Bus\Event\DomainEvents;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMQDomainEventBus implements DomainEventBus
{
    public const CONTENT_TYPE = 'application/json';
    public const CONTENT_ENCODING = 'utf-8';
    public string $exchangeName;
    public RabbitMQConnection $connection;

    public function __construct(
        ?string $exchangeName = null,
        ?RabbitMQConnection $connection = null
    ) {
        $this->exchangeName = $exchangeName ?? 'blog';
        $this->connection = $connection ?? new RabbitMQConnection();
    }

    public function __destruct()
    {
        $this->connection->close();
    }

    public function dispatch(DomainEvents $events): void
    {
        foreach ($events as $event) {
            try {
                $message = $this->makeAmqpMessage($event);
                $routingKey = RabbitMQTypeFormatter::format($event);
                $this->connection->publish(
                    $message,
                    RabbitMQExchangeNameFormatter::format($this->exchangeName),
                    $routingKey
                );
            } catch (\Throwable $exception) {
                // TODO: LOG ERROR
            }
        }
    }

    private function makeAmqpMessage(DomainEvent $event): AMQPMessage
    {
        $body = RabbitMQMessageSerializer::serialize($event);
        $messageProperties = [
            'message_id' => $event->id()->value(),
            'content_type' => self::CONTENT_TYPE,
            'content_encoding' => self::CONTENT_ENCODING,
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ];
        return new AMQPMessage($body, $messageProperties);
    }
}