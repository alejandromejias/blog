<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;
use DateTimeInterface;

final class RabbitMQMessageSerializer
{
    public static function serialize(DomainEvent $domainEvent): string
    {
        return json_encode([
            'data' => [
                'id' => $domainEvent->id()->value(),
                'type' => $domainEvent::name(),
                'occurredOn' => $domainEvent->occurredOn()->format(DateTimeInterface::ATOM),
                'attributes' => $domainEvent->attributes(),
            ]
        ]);
    }
}