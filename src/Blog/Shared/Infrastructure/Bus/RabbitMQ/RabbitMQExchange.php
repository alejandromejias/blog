<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use PhpAmqpLib\Exchange\AMQPExchangeType;

final class RabbitMQExchange
{
    public function __construct(
        private string $name,
        private string $type = AMQPExchangeType::TOPIC,
        private bool $passive = false,
        private bool $durable = true,
        private bool $autoDelete = false,
    ) {}

    public function name(): string
    {
        return $this->name;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function passive(): bool
    {
        return $this->passive;
    }

    public function durable(): bool
    {
        return $this->durable;
    }

    public function autoDelete(): bool
    {
        return $this->autoDelete;
    }
}