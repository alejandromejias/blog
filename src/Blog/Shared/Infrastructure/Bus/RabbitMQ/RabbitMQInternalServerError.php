<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;



use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\InternalServerErrorException;

class RabbitMQInternalServerError extends InternalServerErrorException
{
    public static function subscriberNotFound(string $subscriberName): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::NOT_FOUND_CODE,
            'subscriber',
            sprintf('Subscriber %s cannot be found by queue name', $subscriberName)
        );
    }
}