<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMQConnection
{
    private AMQPStreamConnection $connection;
    private ?AMQPChannel $channel;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection(
            'rabbitmq',
            5672,
            'guest',
            'guest',
        );
        $this->channel = $this->connection->channel();
    }

    public function channel(): ?AMQPChannel
    {
        return $this->channel;
    }

    public function declareExchange(RabbitMQExchange $exchange): void
    {
        $this->channel->exchange_declare(
            $exchange->name(),
            $exchange->type(),
            $exchange->passive(),
            $exchange->durable(),
            $exchange->autoDelete()
        );
    }

    public function deleteExchange(RabbitMQExchange $exchange): void
    {
        $this->channel->exchange_delete($exchange->name());
    }

    public function declareQueue(RabbitMQQueue $queue): void
    {
        $this->channel->queue_declare(
            $queue->name(),
            $queue->passive(),
            $queue->durable(),
            $queue->exclusive(),
            $queue->autoDelete(),
            $queue->nowait(),
            $queue->arguments() ?? []
        );
    }

    public function deleteQueue(RabbitMQQueue $queue): void
    {
        $this->channel->queue_delete($queue->name());
    }

    public function publish(AMQPMessage $message, string $exchangeName, string $routingKey): void
    {
        $this->channel->basic_publish($message, $exchangeName, $routingKey);
    }

    public function bindQueue(
        RabbitMQQueue $queue,
        RabbitMQExchange $exchange,
        string $routingkey
    ): void {
        $this->channel->queue_bind(
            $queue->name(),
            $exchange->name(),
            $routingkey
        );
    }

    public function consume(string $queueName, callable $callback): void
    {
        try {
            $this->channel->basic_qos(null, 10, null);
            $this->channel->basic_consume($queueName, callback: $callback);
            while ($this->channel->is_consuming()) {
                $this->channel->wait(timeout: 5);
            }
        } catch (AMQPTimeoutException) {
            // Do nothing, it's OK to exit by timeout
        } catch (\Throwable $exception) {
            // TODO: LOG ERROR
        } finally {
            $this->close();
        }
    }

    public function close(): void
    {
        $this->connection->__destruct();
    }
}