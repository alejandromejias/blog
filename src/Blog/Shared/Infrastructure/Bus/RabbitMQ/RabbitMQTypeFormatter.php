<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\RabbitMQ;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;

final class RabbitMQTypeFormatter
{
    public static function format(string|DomainEvent $domainEvent): string
    {
        $queueNameParts = ['company', 'blog', 1, $domainEvent::name()];
        return implode('.', $queueNameParts);
    }

    public static function getEventClassNameFromType(string $type): string
    {
        $typeParts = explode('.', $type);
        $action = ucfirst(array_pop($typeParts));
        $name =ucfirst(array_pop($typeParts));
        return "{$name}{$action}Event";
    }
}