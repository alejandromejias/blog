<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Bus\Subscriber;

use App\Blog\Infrastructure\Post\PostCreatedSubscriber;

final class Subscribers
{
    public function __construct(
       private PostCreatedSubscriber $postCreatedSubscriber
    ) {}

    public function all(): array
    {
        return get_object_vars($this);
    }
}