<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Doctrine;

use App\Blog\Shared\Domain\Email\Email;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class EmailType extends StringType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return new Email($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return (string)$value;
    }

    public function getName(): string
    {
        return 'email';
    }
}