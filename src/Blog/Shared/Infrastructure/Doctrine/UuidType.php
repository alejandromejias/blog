<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

abstract class UuidType extends StringType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        $class = $this->className();
        return new $class($value);
    }

    abstract protected function className(): string;

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return (string)$value;
    }

    public function getName(): string
    {
        return $this->customName();
    }

    abstract protected function customName(): string;
}