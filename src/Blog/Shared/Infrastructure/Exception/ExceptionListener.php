<?php

declare(strict_types=1);

namespace App\Blog\Shared\Infrastructure\Exception;

use App\Blog\Shared\Domain\Exception\BaseException;
use App\Blog\Shared\Domain\Exception\ExceptionResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

final class ExceptionListener
{
    private const NOT_CONTROLLED_EXCEPTION = 'NOT_CONTROLLED_EXCEPTION';
    private const UNDEFINED_ROUTE = 'UNDEFINED_ROUTE';

    public function onKernelException(ExceptionEvent $event)
    {
        $exceptionResponse = $this->makeExceptionResponse($event);
        $response = new JsonResponse(
            $exceptionResponse->output(),
            $exceptionResponse->code()
        );
        $event->setResponse($response);
    }

    private function makeExceptionResponse(ExceptionEvent $event): ExceptionResponse
    {
        $exception = $event->getThrowable();
        return new ExceptionResponse(
            $exception->getCode(),
            $event->getRequest()->attributes->get('_route') ?? self::UNDEFINED_ROUTE,
            $exception instanceof BaseException ? $exception->internalCode() : self::NOT_CONTROLLED_EXCEPTION,
            $exception->getMessage(),
            $exception instanceof BaseException ? $exception->details()->toArray() : []
        );
    }
}