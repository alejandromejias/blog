<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Event;

interface DomainEventBus
{
    public function dispatch(DomainEvents $events): void;
}