<?php

namespace App\Blog\Shared\Domain\Bus\Event;

abstract class DomainEvent
{
    private EventId $id;
    private \DateTimeImmutable $occurredOn;

    public function __construct(
        private array $attributes,
        ?EventId $id = null,
        ?\DateTimeImmutable $occurredOn = null,
    ) {
        $this->id = $id ?: EventId::create();
        $this->occurredOn = $occurredOn ?: new \DateTimeImmutable();
    }

    public static function fromPrimitives(
        string $id,
        string $occurredOn,
        array $attributes
    ): self {
        $id = new EventId($id);
        $occurredOn = new \DateTimeImmutable($occurredOn);
        return new static($attributes, $id, $occurredOn);
    }

    abstract public static function name(): string;

    public function id(): EventId
    {
        return $this->id;
    }

    public function occurredOn(): \DateTimeImmutable
    {
        return $this->occurredOn;
    }

    public function attributes(): array
    {
        return $this->attributes;
    }
}