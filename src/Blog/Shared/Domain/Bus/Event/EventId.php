<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Uuid\Uuid;

final class EventId extends Uuid
{
}