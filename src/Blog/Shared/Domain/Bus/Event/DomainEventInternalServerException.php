<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\InternalServerErrorException;

class DomainEventInternalServerException extends InternalServerErrorException
{
    public static function forbiddenClonation(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::FORBIDDEN_CODE,
            'domainEventPublisher',
            'Cloning is not allowed.'
        );
    }

    public static function forbiddenUnserialization(): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::FORBIDDEN_CODE,
            'domainEventPublisher',
            'Unserialization is not allowed in singleton.'
        );
    }
}