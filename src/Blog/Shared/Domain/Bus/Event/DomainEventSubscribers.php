<?php

namespace App\Blog\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Collection\Collection;

class DomainEventSubscribers extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof DomainEventSubscriber;
    }
}