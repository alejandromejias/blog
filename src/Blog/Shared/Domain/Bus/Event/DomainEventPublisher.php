<?php

namespace App\Blog\Shared\Domain\Bus\Event;

class DomainEventPublisher
{
    private static ?self $instance = null;
    private DomainEvents $events;
    private DomainEventSubscribers $subscribers;

    private function __construct()
    {
        $this->events = new DomainEvents();
        $this->subscribers = new DomainEventSubscribers();
    }

    public static function instance(): self
    {
        if (static::$instance === null) {
            static::$instance = new self;
        }
        return static::$instance;
    }

    public function __clone()
    {
        throw DomainEventInternalServerException::forbiddenClonation();
    }

    public function __wakeup()
    {
        throw DomainEventInternalServerException::forbiddenUnserialization();
    }

    public function subscribe(
        DomainEventSubscriber $subscriber
    ): void {
        $this->subscribers->add($subscriber);
    }

    public function publish(DomainEvent $event): void
    {
        $this->events->add($event);
    }

    public function published(): DomainEvents
    {
        return $this->events;
    }
}
