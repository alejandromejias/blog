<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Event;

interface DomainEventSubscriber
{
    public function handle(DomainEvent $event): void;
    public static function isSubscribedTo(DomainEvent $event): bool;
    public static function subscribedTo(): array;
}