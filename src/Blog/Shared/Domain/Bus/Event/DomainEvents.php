<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Collection\Collection;

final class DomainEvents extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof DomainEvent;
    }
}