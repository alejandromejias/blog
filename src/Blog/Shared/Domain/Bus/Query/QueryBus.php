<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Query;

use App\Blog\Shared\Application\Response\Response;

interface QueryBus
{
    public function handle(Query $query): Response;
}