<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Bus\Command;

interface CommandBus
{
    public function dispatch(Command $command): void;
}