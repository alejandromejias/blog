<?php

namespace App\Blog\Shared\Domain\Collection;

use ArrayIterator;

abstract class Collection implements \Countable, \IteratorAggregate
{
    protected array $items;

    public function __construct(array $items = [])
    {
        $this->ensureAreValidTypesOrFail($items);
        $this->items = $items;
    }

    private function ensureAreValidTypesOrFail(array $items): void
    {
        foreach ($items as $item) {
            $this->ensureIsValidTypeOrFail($item);
        }
    }

    public function add(mixed $item): void
    {
        $this->ensureIsValidTypeOrFail($item);
        $this->items[] = $item;
    }

    private function ensureIsValidTypeOrFail(mixed $item): void
    {
        if (!$this->isInstanceOf($item)) {
            throw CollectionInternalServerException::invalidType($item);
        }
    }

    abstract protected function isInstanceOf(mixed $item): bool;

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    public function isNotEmpty(): bool
    {
        return !empty($this->items);
    }

    public function toArray(): array
    {
        $values = [];
        foreach ($this->items as $item) {
            $values[] = method_exists($item, 'toArray')
                ? $item->toArray()
                : $this->toString($item);
        }
        return $values;
    }

    private function toString(mixed $item): string
    {
        return method_exists($item, '__toString')
            ? $item->__toString()
            : $item->toString();
    }
}