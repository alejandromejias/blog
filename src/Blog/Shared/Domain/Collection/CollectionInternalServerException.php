<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Collection;

use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\InternalServerErrorException;

class CollectionInternalServerException extends InternalServerErrorException
{
    public static function invalidType(mixed $invalidType): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::INVALID_TYPE_CODE,
            'collection',
            sprintf('The item of type %s does not belong to the collection', gettype($invalidType))
        );
    }
}