<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Uuid;

use App\Blog\Shared\Domain\Exception\BadRequestException;
use App\Blog\Shared\Domain\Exception\ExceptionDetailCode;
use App\Blog\Shared\Domain\Exception\ExceptionDetailMessage;

class UuidBadRequestException extends BadRequestException
{
    public static function invalidValue(string $target): self
    {
        return (new self())->addDetail(
            ExceptionDetailCode::INVALID_VALUE_CODE,
            $target,
            ExceptionDetailMessage::INVALID_VALUE_MESSAGE
        );
    }
}