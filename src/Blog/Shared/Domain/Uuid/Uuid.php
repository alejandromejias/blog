<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Uuid;

use Ramsey\Uuid\Uuid as RamseyUuid;

abstract class Uuid
{
    protected string $value;

    public function __construct(string $value)
    {
        $this->ensureIsValidUuidOrFail($value);
        $this->value = $value;
    }

    public function value(): string
    {
        return $this->value;
    }

    protected function ensureIsValidUuidOrFail(string $value): void
    {
        if (!static::isValid($value)) {
            throw UuidBadRequestException::invalidValue('Uuid');
        }
    }

    public static function isValid(string $value): bool
    {
        return RamseyUuid::isValid($value);
    }

    public static function create(): static
    {
        return new static(self::generateUuid());
    }

    protected static function generateUuid(): string
    {
        return RamseyUuid::uuid4()->toString();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}