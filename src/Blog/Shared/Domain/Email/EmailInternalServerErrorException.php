<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Email;

use App\Blog\Shared\Domain\Exception\InternalServerErrorException;

class EmailInternalServerErrorException extends InternalServerErrorException
{
    public static function invalidEmail(mixed $invalidEmail): self
    {
        return self::invalidValue('email', $invalidEmail);
    }
}