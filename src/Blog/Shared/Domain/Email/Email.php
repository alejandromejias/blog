<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Email;

class Email
{
    private string $value;

    public function __construct(string $value)
    {
        $this->ensureIsValidEmailOrFail($value);
        $this->value = $value;
    }

    private function ensureIsValidEmailOrFail(string $value): void
    {
        if (!self::isValid($value)) {
            throw EmailInternalServerErrorException::invalidEmail($value);
        }
    }

    public static function isValid(string $value): bool
    {
        return (bool)filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }
}