<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

final class ExceptionResponse
{
    private int $code;
    public function __construct(
        int $code,
        private string $tag,
        private string $target,
        private string $message,
        private array $details
    ) {
        $this->code = $code ?: 500;
    }

    public function code(): int
    {
        return $this->code;
    }

    public function output(): array
    {
        $output = [
            'tag' => $this->tag,
            'target' => $this->target,
            'message' => $this->message,
        ];
        if ($this->details) {
            $output['details'] = $this->details;
        }
        return $output;
    }
}