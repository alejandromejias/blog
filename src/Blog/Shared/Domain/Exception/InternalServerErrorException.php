<?php

namespace App\Blog\Shared\Domain\Exception;

class InternalServerErrorException extends BaseException
{
    protected const INTERNAL_SERVER_ERROR_HTTP_CODE = 500;
    protected const INTERNAL_CODE = 'INTERNAL_SERVER_ERROR';
    protected const MESSAGE = 'Unexpected error has occurred. Please contact support if the error persists.';

    public function __construct(
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) {
        $severity = Severity::ERROR;
        $internalCode = static::INTERNAL_CODE;
        $message = static::MESSAGE;
        $code = self::INTERNAL_SERVER_ERROR_HTTP_CODE;
        parent::__construct($severity, $internalCode, $message, $code, $previous, $details);
    }

    public static function invalidValue(string $target, mixed $invalidValue): static
    {
        return (new static())->addDetail(
            ExceptionDetailCode::INVALID_VALUE_CODE,
            $target,
            sprintf('%s is a invalid %s value', $invalidValue, $target)
        );
    }
}