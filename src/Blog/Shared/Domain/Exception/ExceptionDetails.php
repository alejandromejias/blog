<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Collection\Collection;

final class ExceptionDetails extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof ExceptionDetail;
    }
}