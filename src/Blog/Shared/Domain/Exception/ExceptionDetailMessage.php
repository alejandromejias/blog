<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

class ExceptionDetailMessage
{
    public const NULL_VALUE_MESSAGE = 'This value is required.';
    public const INVALID_VALUE_MESSAGE = 'This value is invalid.';
}