<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

final class ExceptionDetail
{
    public function __construct(
        private string $code,
        private string $target,
        private string $message
    ) {}

    public function code(): string
    {
        return $this->code;
    }

    public function target(): string
    {
        return $this->target;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}