<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

class UnauthorizedException extends BaseException
{
    protected const UNAUTHORIZED_HTTP_CODE = 401;
    protected const INTERNAL_CODE = 'UNAUTHORIZED';
    protected const MESSAGE = 'Unauthorized action.';

    public function __construct(
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) {
        $severity = Severity::WARNING;
        $internalCode = static::INTERNAL_CODE;
        $message = static::MESSAGE;
        $code = self::UNAUTHORIZED_HTTP_CODE;
        parent::__construct($severity, $internalCode, $message, $code, $previous, $details);
    }
}