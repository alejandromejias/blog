<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

abstract class BaseException extends \Exception
{
    protected ExceptionDetails $details;

    public function __construct(
        protected string $severity,
        protected string $internalCode,
        string $message,
        int $code,
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->details = $details ?? new ExceptionDetails();
    }

    public function severity(): string
    {
        return $this->severity;
    }

    public function internalCode(): string
    {
        return $this->internalCode;
    }

    public function details(): ExceptionDetails
    {
        return $this->details;
    }

    public function addDetail(string $code, string $target, string $message): self
    {
        $this->details->add($this->makeExceptionDetail($code, $target, $message));
        return $this;
    }

    private function makeExceptionDetail(string $code, string $target, string $message): ExceptionDetail
    {
        return new ExceptionDetail($code, $target, $message);
    }
}