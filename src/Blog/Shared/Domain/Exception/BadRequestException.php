<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

class BadRequestException extends BaseException
{
    protected const BAD_REQUEST_HTTP_CODE = 400;
    protected const INTERNAL_CODE = 'INVALID_OR_NULL_VALUE';
    protected const MESSAGE = 'Invalid or null parameter(s) in the request.';

    public function __construct(
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) {
        $severity = Severity::WARNING;
        $internalCode = static::INTERNAL_CODE;
        $message = static::MESSAGE;
        $code = self::BAD_REQUEST_HTTP_CODE;
        parent::__construct($severity, $internalCode, $message, $code, $previous, $details);
    }
}