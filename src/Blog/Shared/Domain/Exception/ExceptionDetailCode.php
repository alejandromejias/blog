<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

class ExceptionDetailCode
{
    public const NULL_VALUE_CODE = 'NULL_VALUE_ERROR';
    public const INVALID_VALUE_CODE = 'INVALID_VALUE_ERROR';
    public const INVALID_TYPE_CODE = 'INVALID_TYPE_ERROR';
    public const UNAUTHORIZED_CODE = 'UNAUTHORIZED_ERROR';
    public const FORBIDDEN_CODE = 'FORBIDDEN_ERROR';
    public const NOT_FOUND_CODE = 'NOT_FOUND_ERROR';
}