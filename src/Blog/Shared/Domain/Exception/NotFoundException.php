<?php

declare(strict_types=1);

namespace App\Blog\Shared\Domain\Exception;

class NotFoundException extends BaseException
{
    protected const NOT_FOUND_HTTP_CODE = 404;
    protected const INTERNAL_CODE = 'NOT_FOUND';
    protected const MESSAGE = 'The requested resource has not been found.';

    public function __construct(
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) {
        $severity = Severity::WARNING;
        $internalCode = static::INTERNAL_CODE;
        $message = static::MESSAGE;
        $code = self::NOT_FOUND_HTTP_CODE;
        parent::__construct($severity, $internalCode, $message, $code, $previous, $details);
    }
}