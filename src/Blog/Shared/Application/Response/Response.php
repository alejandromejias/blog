<?php

declare(strict_types=1);

namespace App\Blog\Shared\Application\Response;

interface Response
{
    public function toArray(): array;
}