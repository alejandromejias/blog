<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Command\RabbitMQ;

use App\Blog\Shared\Infrastructure\Bus\RabbitMQ\RabbitMQConfigurator;
use App\Blog\Shared\Infrastructure\Bus\Subscriber\Subscribers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class RabbitMQConfigureCommand extends Command
{
    public function __construct(
        private RabbitMQConfigurator $configurator,
        private Subscribers $subscribers,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected static $defaultName = 'rabbitmq:configure';

    protected function configure(): void
    {
        $this->setDescription('Set RabbitMQ configuration');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $exchangeName = 'blog';
        ($this->configurator)($exchangeName,  $this->subscribers->all());
        $io = new SymfonyStyle($input, $output);
        $io->success('RabbitMQ has been configured successfully!');
        return Command::SUCCESS;
    }
}