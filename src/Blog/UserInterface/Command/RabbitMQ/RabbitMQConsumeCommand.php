<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Command\RabbitMQ;

use App\Blog\Shared\Infrastructure\Bus\RabbitMQ\RabbitMQConsumer;
use App\Blog\Shared\Infrastructure\Bus\RabbitMQ\RabbitMQQueueNameFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class RabbitMQConsumeCommand extends Command
{
    public function __construct(
        private RabbitMQConsumer $consumer,
        private RabbitMQQueueNameFormatter $queueNameFormatter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected static $defaultName = 'rabbitmq:consume';

    protected function configure(): void
    {
        $this->setDescription('Consume RabbitMQ queues');
        $this->addArgument('queue', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $queueName = $input->getArgument('queue');
        $subscriber = $this->queueNameFormatter->getSubscriberFromQueueName($queueName);
        $this->consumer->consume($subscriber, $queueName);
        $io->success(
            sprintf('RabbitMQ has consumed queue %s successfully!', $queueName)
        );
        return Command::SUCCESS;
    }
}