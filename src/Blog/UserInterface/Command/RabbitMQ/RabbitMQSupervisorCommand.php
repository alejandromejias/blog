<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Command\RabbitMQ;

use App\Blog\Shared\Infrastructure\Bus\RabbitMQ\RabbitMQSupervisor;
use App\Blog\Shared\Infrastructure\Bus\Subscriber\Subscribers;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RabbitMQSupervisorCommand extends Command
{
    protected static $defaultName = 'rabbitmq:supervisor';

    public function __construct(
        private Subscribers $subscribers,
        private RabbitMQSupervisor $supervisor,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setDescription('Set Supervisor configuration');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->subscribers->all() as $subscriber) {
            ($this->supervisor)($subscriber);
        }
        $io = new SymfonyStyle($input, $output);
        $io->success('Supervisor has been configured successfully!');
        return Command::SUCCESS;
    }
}