<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Middleware;

use App\Blog\Domain\User\UserUnauthorizedException;

class AuthenticationFailureMiddleware
{
    public function invalidCredentials(): never
    {
        throw UserUnauthorizedException::invalidCredentials();
    }

    public function invalidJWT(): never
    {
        throw UserUnauthorizedException::invalidJWT();
    }

    public function notFoundJWT(): never
    {
        throw UserUnauthorizedException::notFoundJWT();
    }

    public function expiredJWT(): never
    {
        throw UserUnauthorizedException::expiredJWT();
    }
}