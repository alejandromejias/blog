<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    protected function accepted(): Response
    {
        return new Response(status: Response::HTTP_ACCEPTED);
    }
}