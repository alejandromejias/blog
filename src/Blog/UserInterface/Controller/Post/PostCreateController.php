<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Post;

use App\Blog\Application\Post\Command\PostCreateCommand;
use App\Blog\Shared\Domain\Bus\Command\CommandBus;
use App\Blog\UserInterface\Controller\ApiController;
use App\Blog\UserInterface\Controller\Validation\Post\PostCreateValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class PostCreateController extends ApiController
{
    public function __construct(
        private CommandBus $commandBus,
        private PostCreateValidator $validator
    ) {}

    public function __invoke(Request $request): Response
    {
        $this->validator->validateOrFail($request);
        $postCreateCommand = new PostCreateCommand(
            $request->get('author'),
            $request->get('title'),
            $request->get('content'),
        );
        $this->commandBus->dispatch($postCreateCommand);
        return $this->accepted();
    }
}
