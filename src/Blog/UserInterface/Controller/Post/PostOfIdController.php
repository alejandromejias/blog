<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Post;

use App\Blog\Application\Post\Query\PostOfIdQuery;
use App\Blog\Shared\Domain\Bus\Query\QueryBus;
use App\Blog\UserInterface\Controller\ApiController;
use App\Blog\UserInterface\Controller\Validation\Post\PostOfIdValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class PostOfIdController extends ApiController
{
    public function __construct(
        private QueryBus $queryBus,
        private PostOfIdValidator $validator
    ) {}

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validateOrFail($request);
        $id = $request->get('id');
        $postOfIdQuery = new PostOfIdQuery($id);
        $response = $this->queryBus->handle($postOfIdQuery);
        return $this->json($response->toArray());
    }
}