<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\User;

use App\Blog\Application\User\Command\UserCreateCommand;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRepository;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Infrastructure\Auth\Auth;
use App\Blog\Shared\Domain\Bus\Command\CommandBus;
use App\Blog\Shared\Domain\Email\Email;
use App\Blog\UserInterface\Controller\ApiController;
use App\Blog\UserInterface\Controller\Validation\User\UserCreateValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserCreateController extends ApiController
{
    public function __construct(
        private CommandBus $commandBus,
        private UserCreateValidator $validator,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $this->validator->validateOrFail($request);
        $userId = $this->userRepository->nextIdentity();
        $userCreateCommand = new UserCreateCommand(
            $userId->value(),
            $request->get('username'),
            $passwordHasher->hashPassword(
                $this->auth($request, $userId),
                $request->get('password')
            ),
            $request->get('roles'),
        );
        $this->commandBus->dispatch($userCreateCommand);
        return $this->accepted();
    }

    private function auth(Request $request, UserId $userId): Auth
    {
        return new Auth(
            $userId,
            $request->get('username'),
            'undefined',
            UserRoles::createFromPrimitives($request->get('roles'))
        );
    }
}