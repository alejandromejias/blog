<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation\User;

use App\Blog\Shared\Infrastructure\Validation\Validator;
use App\Blog\UserInterface\Controller\Validation\EmailValidationStrategy;
use App\Blog\UserInterface\Controller\Validation\NotNullValidationStrategy;
use App\Blog\UserInterface\Controller\Validation\User\UserRolesValidationStrategy;
use Symfony\Component\HttpFoundation\Request;

class UserCreateValidator extends Validator
{
    public function validateOrFail(Request $request): void
    {
        $this->context->addValidationStrategy(
            new EmailValidationStrategy($request->get('username'), 'username')
        );
        $this->context->addValidationStrategy(
            new NotNullValidationStrategy($request->get('password'), 'password')
        );
        $this->context->addValidationStrategy(
            new UserRolesValidationStrategy($request->get('roles'))
        );
        $this->context->validateOrFail();

    }
}