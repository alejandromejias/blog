<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation\User;

use App\Blog\Domain\User\UserRole;
use App\Blog\Shared\Infrastructure\Validation\ValidationErrorDetail;
use App\Blog\Shared\Infrastructure\Validation\ValidationStrategy;

class UserRolesValidationStrategy extends ValidationStrategy
{
    public function __construct(mixed $userRoles)
    {
        parent::__construct();
        $this->checkIsValidUserRoles($userRoles);
    }

    private function checkIsValidUserRoles(mixed $userRoles): void
    {
        if (is_null($userRoles)) {
            $this->errors->add(ValidationErrorDetail::createFromNullValue('roles'));
            return;
        }
        if (!is_array($userRoles) || $this->hasInvalidUserRoles($userRoles)) {
            $this->errors->add(ValidationErrorDetail::createFromInvalidValue('roles'));
        }
    }

    private function hasInvalidUserRoles(array $userRoles): bool
    {
        return (bool)array_filter($userRoles, function(string $userRole) {
            return is_null(UserRole::tryFrom($userRole));
        });
    }
}