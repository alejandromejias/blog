<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation;

use App\Blog\Shared\Infrastructure\Validation\ValidationErrorDetail;
use App\Blog\Shared\Infrastructure\Validation\ValidationStrategy;

class NotNullValidationStrategy extends ValidationStrategy
{
    public function __construct(mixed $value, string $target)
    {
        parent::__construct();
        if (is_null($value)) {
            $this->errors->add(ValidationErrorDetail::createFromNullValue($target));
        }
    }
}