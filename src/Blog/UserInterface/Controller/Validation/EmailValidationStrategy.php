<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation;

use App\Blog\Shared\Domain\Email\Email;
use App\Blog\Shared\Infrastructure\Validation\ValidationErrorDetail;
use App\Blog\Shared\Infrastructure\Validation\ValidationStrategy;

class EmailValidationStrategy extends ValidationStrategy
{
    public function __construct(mixed $email, string $target = 'email')
    {
        parent::__construct();
        $this->checkIsValidEmail($email, $target);
    }

    private function checkIsValidEmail(mixed $email, string $target): void
    {
        if (is_null($email)) {
            $this->errors->add(ValidationErrorDetail::createFromNullValue($target));
            return;
        }
        if (!Email::isValid($email)) {
            $this->errors->add(ValidationErrorDetail::createFromInvalidValue($target));
        }
    }
}