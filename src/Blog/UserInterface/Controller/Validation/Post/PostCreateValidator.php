<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation\Post;

use App\Blog\Shared\Infrastructure\Validation\Validator;
use App\Blog\UserInterface\Controller\Validation\NotNullValidationStrategy;
use Symfony\Component\HttpFoundation\Request;

class PostCreateValidator extends Validator
{
    public function validateOrFail(Request $request): void
    {
        $this->context->addValidationStrategy(
            new NotNullValidationStrategy($request->get('author'), 'author')
        );
        $this->context->addValidationStrategy(
            new NotNullValidationStrategy($request->get('title'), 'title')
        );
        $this->context->addValidationStrategy(
            new NotNullValidationStrategy($request->get('content'), 'content')
        );
        $this->context->validateOrFail();
    }
}