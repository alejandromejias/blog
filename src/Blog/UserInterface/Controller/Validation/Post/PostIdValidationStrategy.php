<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation\Post;

use App\Blog\Domain\Post\PostId;
use App\Blog\Shared\Infrastructure\Validation\ValidationErrorDetail;
use App\Blog\Shared\Infrastructure\Validation\ValidationStrategy;

class PostIdValidationStrategy extends ValidationStrategy
{
    public function __construct(mixed $email)
    {
        parent::__construct();
        $this->checkIsValidPostId($email);
    }

    private function checkIsValidPostId(mixed $postId): void
    {
        if (is_null($postId)) {
            $this->errors->add(ValidationErrorDetail::createFromNullValue('postId'));
            return;
        }
        if (!PostId::isValid($postId)) {
            $this->errors->add(ValidationErrorDetail::createFromInvalidValue('postId'));
        }
    }
}