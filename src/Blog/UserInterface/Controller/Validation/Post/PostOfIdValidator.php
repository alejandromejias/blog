<?php

declare(strict_types=1);

namespace App\Blog\UserInterface\Controller\Validation\Post;

use App\Blog\Shared\Infrastructure\Validation\Validator;
use App\Blog\UserInterface\Controller\Validation\Post\PostIdValidationStrategy;
use Symfony\Component\HttpFoundation\Request;

class PostOfIdValidator extends Validator
{
    public function validateOrFail(Request $request): void
    {
        $this->context->addValidationStrategy(
            new PostIdValidationStrategy($request->get('id'))
        );
        $this->context->validateOrFail();
    }
}