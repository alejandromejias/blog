<?php

declare(strict_types=1);

namespace App\Tests\Shared\Infrastructure\Doctrine;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;

trait SetupDatabaseTrait
{
    protected static function runDoctrineDatabase(): void
    {
        $application = new Application(static::kernel());
        $application->setAutoExit(false);
        foreach (self::doctrineDatabaseCommands() as $command) {
            $application->run(new ArrayInput($command), new NullOutput());
        }
    }

    private static abstract function kernel(): KernelInterface;

    private static function doctrineDatabaseCommands(): array
    {
        return [
            [
                'command' => 'doctrine:database:create',
                '--env' => 'test'
            ],
            [
                'command' => 'doctrine:schema:create',
                '--env' => 'test'
            ]
        ];
    }
}