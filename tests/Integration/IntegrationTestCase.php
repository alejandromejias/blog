<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\Tests\Shared\Infrastructure\Doctrine\SetupDatabaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class IntegrationTestCase extends KernelTestCase
{
    use SetupDatabaseTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        self::runDoctrineDatabase();
    }

    protected function service(string $id):  mixed
    {
        return static::getContainer()->get($id);
    }

    private static function kernel(): KernelInterface
    {
        return static::$kernel;
    }
}