<?php

declare(strict_types=1);

namespace App\Tests\Integration\Infrastructure\Post;

use App\Blog\Domain\Post\PostRepository;
use App\Tests\Integration\IntegrationTestCase;
use App\Tests\Unit\Domain\Post\PostMother;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DoctrinePostRepositoryTest extends IntegrationTestCase
{
    private PostRepository $postRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postRepository = $this->service(PostRepository::class);
    }

    /**
     * @test
     */
    public function it_should_save_a_post()
    {
        $post = PostMother::random();

        $this->postRepository->save($post);

        $this->assertEquals($post, $this->postRepository->ofId($post->id()));
    }
}