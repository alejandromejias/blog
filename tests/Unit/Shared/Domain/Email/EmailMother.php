<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Email;

use App\Blog\Shared\Domain\Email\Email;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class EmailMother
{
    public static function create(string $email): Email
    {
        return new Email($email);
    }

    public static function random(): Email
    {
        return self::create(WordMother::email());
    }
}