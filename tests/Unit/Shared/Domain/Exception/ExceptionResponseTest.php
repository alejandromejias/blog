<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exception;

use PHPUnit\Framework\TestCase;

class ExceptionResponseTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_an_exception_response_with_default_http_code()
    {
        $exceptionResponse = ExceptionResponseMother::random(['code' => 0]);

        $this->assertEquals(500, $exceptionResponse->code());
    }

    /**
     * @test
     */
    public function it_should_get_exception_response_with_details()
    {
        $exceptionResponse = ExceptionResponseMother::random();

        $output = $exceptionResponse->output();

        $this->assertArrayHasKey('details', $output);
    }

    /**
     * @test
     */
    public function it_should_get_exception_response_without_details()
    {
        $exceptionResponse = ExceptionResponseMother::random(['details' => []]);

        $output = $exceptionResponse->output();

        $this->assertArrayNotHasKey('details', $output);
    }
}