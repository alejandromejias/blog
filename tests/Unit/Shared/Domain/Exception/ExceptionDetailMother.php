<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\ExceptionDetail;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class ExceptionDetailMother
{
    public static function create(string $code, string $target, string $message): ExceptionDetail
    {
        return new ExceptionDetail($code, $target, $message);
    }

    public static function random(): ExceptionDetail
    {
        return static::create(
            WordMother::word(),
            WordMother::word(),
            WordMother::sentence()
        );
    }
}