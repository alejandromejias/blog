<?php

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\ExceptionDetails;

class ExceptionDetailsMother
{
    public static function create(array $items = []): ExceptionDetails
    {
        return new ExceptionDetails($items);
    }

    public static function random(int $elements = 1): ExceptionDetails
    {
        $items = [];
        for ($i = 0; $i < $elements; $i++) {
            $items[] = ExceptionDetailMother::random();
        }
        return static::create($items);
    }
}