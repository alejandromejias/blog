<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\BaseException;
use App\Blog\Shared\Domain\Exception\ExceptionDetails;
use App\Tests\Unit\Shared\Domain\Mother\NumberMother;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class BaseExceptionMother
{
    public static function create(
        string $severity,
        string $internalCode,
        string $message,
        int $code,
        ?\Throwable $previous = null,
        ?ExceptionDetails $details = null
    ) : BaseException {
        return new BaseExceptionStub(
            $severity,
            $internalCode,
            $message,
            $code,
            $previous,
            $details
        );
    }

    public static function random(): BaseException
    {
        return static::create(
            SeverityMother::random(),
            WordMother::word(),
            WordMother::sentence(),
            NumberMother::code(),
        );
    }
}