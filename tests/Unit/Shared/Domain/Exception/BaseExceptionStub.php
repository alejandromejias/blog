<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\BaseException;

class BaseExceptionStub extends BaseException
{
}