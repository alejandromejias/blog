<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\ExceptionDetails;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;
use PHPUnit\Framework\TestCase;

class BaseExceptionTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_an_exception_with_empty_exception_details_by_default()
    {
        $exception = BaseExceptionMother::random();

        $this->assertInstanceOf(ExceptionDetails::class, $exception->details());
        $this->assertTrue($exception->details()->isEmpty());
    }

    /**
     * @test
     */
    public function it_should_add_exception_detail_in_exception()
    {
        $exception = BaseExceptionMother::random();

        $exception->addDetail(
            WordMother::word(),
            WordMother::word(),
            WordMother::sentence()
        );

        $this->assertCount(1, $exception->details());
    }
}