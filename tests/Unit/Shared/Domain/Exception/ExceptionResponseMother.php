<?php

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\ExceptionResponse;
use App\Tests\Unit\Shared\Domain\Mother\NumberMother;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class ExceptionResponseMother
{
    public static function create(
        int $code,
        string $tag,
        string $target,
        string $message,
        array $details
    ): ExceptionResponse {
        return new ExceptionResponse(
            $code,
            $tag,
            $target,
            $message,
            $details
        );
    }

    public static function random(array $parameters = []): ExceptionResponse
    {
        $arguments = [
            'code' => $parameters['code'] ?? NumberMother::code(),
            'tag' => $parameters['tag'] ?? WordMother::word(),
            'target' => $parameters['target'] ?? WordMother::word(),
            'message' => $parameters['message'] ?? WordMother::word(),
            'details' => $parameters['details'] ?? ExceptionDetailsMother::random()->toArray(),
        ];
        return static::create(...array_values($arguments));
    }
}