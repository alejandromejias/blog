<?php

namespace App\Tests\Unit\Shared\Domain\Exception;

use App\Blog\Shared\Domain\Exception\Severity;
use App\Tests\Unit\Shared\Domain\Mother\FactoryMother;

class SeverityMother
{
    public static function random(): string
    {
        return FactoryMother::create()->randomElement([
            Severity::DEBUG,
            Severity::INFO,
            Severity::NOTICE,
            Severity::WARNING,
            Severity::ERROR,
            Severity::CRITICAL,
            Severity::ALERT,
            Severity::EMERGENCY,
        ]);
    }
}