<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Uuid;

use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class UuidStubMother
{
    public static function create(string $value): UuidStub
    {
        return new UuidStub($value);
    }

    public static function random(): UuidStub
    {
        return static::create(WordMother::uuid());
    }
}