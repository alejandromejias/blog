<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Uuid;

use App\Blog\Shared\Domain\Uuid\Uuid;

class UuidStub extends Uuid
{
}