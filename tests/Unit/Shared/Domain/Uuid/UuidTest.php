<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Uuid;

use App\Blog\Shared\Domain\Uuid\Uuid;
use App\Blog\Shared\Domain\Uuid\UuidBadRequestException;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;
use PHPUnit\Framework\TestCase;

class UuidTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_an_instance_from_external_value()
    {
        $value = WordMother::uuid();

        $uuid = new UuidStub($value);

        $this->assertInstanceOf(Uuid::class, $uuid);
    }

    /**
     * @test
     */
    public function it_should_create_an_instance_from_create_method()
    {
        $uuid = UuidStub::create();

        $this->assertInstanceOf(Uuid::class, $uuid);
    }

    /**
     * @test
     */
    public function it_should_throw_an_exception_when_invalid_external_value_is_provided()
    {
        $invalidValue = WordMother::word();

        $this->expectException(UuidBadRequestException::class);

        (new UuidStub($invalidValue));
    }
}