<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Bus\Event\EventId;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class EventIdMother
{
    public static function create(string $value): EventId
    {
        return new EventId($value);
    }

    public static function random(): EventId
    {
        return static::create(WordMother::uuid());
    }
}