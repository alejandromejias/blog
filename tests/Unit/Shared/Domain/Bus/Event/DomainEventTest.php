<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;
use App\Blog\Shared\Domain\Bus\Event\EventId;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;
use PHPUnit\Framework\TestCase;

class DomainEventTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_a_domain_event()
    {
        $eventId = EventIdMother::random();
        $occurredOn = new \DateTimeImmutable();

        $domain = new DomainEventStub(
            WordMother::array(),
            $eventId,
            $occurredOn
        );

        $this->assertEquals($eventId, $domain->id());
        $this->assertEquals($occurredOn, $domain->occurredOn());
    }

    /**
     * @test
     */
    public function it_should_create_a_domain_event_with_only_required_params()
    {
        $domain = new DomainEventStub(
            WordMother::array()
        );

        $this->assertInstanceOf(EventId::class, $domain->id());
        $this->assertInstanceOf(\DateTimeImmutable::class, $domain->occurredOn());
    }

    /**
     * @test
     */
    public function it_should_create_a_domain_event_from_primitives()
    {
        $event = DomainEventStub::fromPrimitives(
            (string)EventIdMother::random(),
            (new \DateTimeImmutable())->format(\DateTimeImmutable::ATOM),
            WordMother::array()
        );

        $this->assertInstanceOf(DomainEvent::class, $event);
    }
}