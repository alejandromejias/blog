<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Bus\Event;

use App\Blog\Shared\Domain\Bus\Event\DomainEvent;

class DomainEventStub extends DomainEvent
{
    public static function name(): string
    {
        return 'event.stubbed';
    }
}