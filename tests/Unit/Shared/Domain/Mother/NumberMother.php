<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Mother;

use Symfony\Component\HttpFoundation\Response;

class NumberMother
{
    public static function code(): int
    {
        return FactoryMother::create()->randomElement([
            Response::HTTP_OK,
            Response::HTTP_BAD_REQUEST,
            Response::HTTP_INTERNAL_SERVER_ERROR
        ]);
    }
}