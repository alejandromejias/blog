<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Mother;

use Faker\Factory;
use Faker\Generator;

class FactoryMother
{
    private static ?Generator $instance = null;

    public static function create(): Generator
    {
        if (static::$instance === null) {
            static::$instance = Factory::create();
        }
        return static::$instance;
    }
}