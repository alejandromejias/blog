<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Mother;

class WordMother
{
    public static function word(): string
    {
        return FactoryMother::create()->word();
    }

    public static function sentence(): string
    {
        return FactoryMother::create()->sentence();
    }

    public static function text(): string
    {
        return FactoryMother::create()->text();
    }

    public static function name(): string
    {
        return FactoryMother::create()->name();
    }

    public static function uuid(): string
    {
        return FactoryMother::create()->uuid();
    }

    public static function password():  string
    {
        return FactoryMother::create()->password();
    }

    public static function email(): string
    {
        return FactoryMother::create()->email();
    }

    public static function array(): array
    {
        return FactoryMother::create()->randomElements([
            self::word(),
            self::word(),
            self::word(),
        ], 3);
    }
}