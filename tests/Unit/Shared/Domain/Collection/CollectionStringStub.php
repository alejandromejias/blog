<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Collection;

use App\Blog\Shared\Domain\Collection\Collection;
use App\Tests\Unit\Shared\Domain\Uuid\UuidStub;

class CollectionStringStub extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof UuidStub;
    }
}