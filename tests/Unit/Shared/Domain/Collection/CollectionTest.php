<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain\Collection;

use App\Blog\Shared\Domain\Collection\Collection;
use App\Blog\Shared\Domain\Collection\CollectionInternalServerException;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;
use App\Tests\Unit\Shared\Domain\Uuid\UuidStubMother;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_an_instance_according_to_collection_type()
    {
        $item = UuidStubMother::random();

        $collection = new CollectionStringStub([$item]);

        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertCount(1, $collection);
    }

    /**
     * @test
     */
    public function it_should_throw_an_exception_when_an_invalid_item_collection_type_is_provided_by_the_constructor()
    {
        $invalidItem = WordMother::word();

        $this->expectException(CollectionInternalServerException::class);

        (new CollectionStringStub([$invalidItem]));
    }

    /**
     * @test
     */
    public function it_should_add_a_new_item_to_collection()
    {
        $item = UuidStubMother::random();
        $collection = new CollectionStringStub();

        $collection->add($item);

        $this->assertCount(1, $collection);
    }

    /**
     * @test
     */
    public function it_should_throw_an_exception_when_an_invalid_item_collection_type_is_provided_by_add_method()
    {
        $invalidItem = WordMother::word();
        $collection = new CollectionStringStub();

        $this->expectException(CollectionInternalServerException::class);

        $collection->add($invalidItem);
    }

    /**
     * @test
     */
    public function it_should_count_number_items_of_collection()
    {
        $firstItem = UuidStubMother::random();
        $secondItem = clone $firstItem;

        $collection = new CollectionStringStub([$firstItem, $secondItem]);

        $this->assertEquals(2, $collection->count());
    }

    /**
     * @test
     */
    public function it_should_assert_is_an_empty_collection()
    {
        $collection = new CollectionStringStub();

        $this->assertTrue($collection->isEmpty());
        $this->assertFalse($collection->isNotEmpty());
    }

    /**
     * @test
     */
    public function it_should_assert_is_not_an_empty_collection()
    {
        $item = UuidStubMother::random();
        $collection = new CollectionStringStub([$item]);

        $this->assertTrue($collection->isNotEmpty());
        $this->assertFalse($collection->isEmpty());
    }

    /**
     * @test
     */
    public function it_should_transform_collection_object_with_to_string_method_to_array()
    {
        $item = UuidStubMother::random();
        $collection = new CollectionStringStub([$item]);

        $collectionToArray = $collection->toArray();

        $this->assertEquals($item->value(), $collectionToArray[0]);
    }

    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function it_should_transform_collection_object_with_to_array_method_to_array()
    {
        // TODO: use an stub class with toArray method
    }

    /**
     * @test
     */
    public function it_should_transform_empty_collection_object_to_empty_array()
    {
        $collection = new CollectionStringStub();

        $collectionToArray = $collection->toArray();

        $this->assertEmpty($collectionToArray);
    }
}