<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Post;

use App\Blog\Domain\Post\Post;
use App\Blog\Domain\Post\PostId;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class PostMother
{
    public static function create(
        PostId $id,
        string $author,
        string $title,
        string $content
    ): Post {
        return new Post(
            $id,
            $author,
            $title,
            $content
        );
    }

    public static function random(array $parameters = []): Post
    {
        $arguments = [
            'id' => $parameters['id'] ?? PostIdMother::random(),
            'author' => $parameters['author'] ?? WordMother::name(),
            'title' => $parameters['title'] ?? WordMother::sentence(),
            'content' => $parameters['content'] ?? WordMother::text(),
        ];
        return static::create(...array_values($arguments));
    }
}