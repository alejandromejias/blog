<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Post;

use App\Blog\Domain\Post\PostId;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;

class PostIdMother
{
    public static function create(string $value): PostId
    {
        return new PostId($value);
    }

    public static function random(): PostId
    {
        return static::create(WordMother::uuid());
    }
}