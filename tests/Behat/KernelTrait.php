<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use LogicException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Service\ResetInterface;

trait KernelTrait
{
    protected static $class;
    protected static $kernel;
    protected static $booted = false;
    private static ?ContainerInterface $kernelContainer = null;

    protected static function bootKernel(array $options = []): KernelInterface
    {
        static::ensureKernelShutdown();
        static::$kernel = static::createKernel($options);
        static::$kernel->boot();
        static::$booted = true;
        self::$kernelContainer = static::$kernel->getContainer();
        return static::$kernel;
    }

    protected static function ensureKernelShutdown()
    {
        if (null !== static::$kernel) {
            static::$kernel->shutdown();
            static::$booted = false;
        }
        if (self::$kernelContainer instanceof ResetInterface) {
            self::$kernelContainer->reset();
        }
        self::$kernelContainer = null;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        if (null === static::$class) {
            static::$class = static::kernelClass();
        }
        return new static::$class(self::env($options), self::debug($options));
    }

    private static function env(array $options): mixed
    {
        if (isset($options['environment'])) {
            return $options['environment'];
        }
        if (isset($_ENV['APP_ENV'])) {
            return $_ENV['APP_ENV'];
        }
        if (isset($_SERVER['APP_ENV'])) {
            return $_SERVER['APP_ENV'];
        }
        return 'test';
    }

    private static function debug(array $options): mixed
    {
        if (isset($options['debug'])) {
            return $options['debug'];
        }
        if (isset($_ENV['APP_DEBUG'])) {
            return $_ENV['APP_DEBUG'];
        }
        if (isset($_SERVER['APP_DEBUG'])) {
            return $_SERVER['APP_DEBUG'];
        }
        return true;
    }

    protected static function kernelClass(): string
    {
        if (!isset($_SERVER['KERNEL_CLASS']) && !isset($_ENV['KERNEL_CLASS'])) {
            throw new LogicException('You must set the KERNEL_CLASS environment variable.');
        }
        if (!class_exists($class = $_ENV['KERNEL_CLASS'] ?? $_SERVER['KERNEL_CLASS'])) {
            throw new \RuntimeException(sprintf('Class "%s" does not exist or cannot be autoloaded.', $class));
        }
        return $class;
    }

    protected static function container(): ContainerInterface
    {
        if (!static::$booted) {
            static::bootKernel();
        }
        try {
            return self::$kernelContainer->get('test.service_container');
        } catch (ServiceNotFoundException $exception) {
            throw new LogicException('Could not find service "test.service_container', previous: $exception);
        }
    }
}