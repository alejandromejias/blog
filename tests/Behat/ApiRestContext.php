<?php

namespace App\Tests\Behat;

use App\Blog\Domain\User\User;
use App\Blog\Domain\User\UserId;
use App\Blog\Domain\User\UserRepository;
use App\Blog\Domain\User\UserRole;
use App\Blog\Domain\User\UserRoles;
use App\Blog\Infrastructure\Auth\Auth;
use App\Blog\Shared\Domain\Email\Email;
use App\Tests\Shared\Infrastructure\Doctrine\SetupDatabaseTrait;
use App\Tests\Unit\Shared\Domain\Email\EmailMother;
use App\Tests\Unit\Shared\Domain\Mother\WordMother;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use cebe\openapi\Reader;
use cebe\openapi\spec\OpenApi;
use cebe\openapi\spec\Server;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Osteel\OpenApi\Testing\ValidatorBuilder;
use Osteel\OpenApi\Testing\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use PHPUnit\Framework\Assert as Assertions;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;

abstract class ApiRestContext implements Context
{
    use KernelTrait;
    use SetupDatabaseTrait;

    private Request $request;
    private Response $response;
    private string $serverUrl;
    private array $authorization;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private RouterInterface $router,
        private UserPasswordHasherInterface $passwordHasher,
        private UserRepository $userRepository,
        private JWTTokenManagerInterface $JWTTokenManager
    ) {
        self::bootKernel();
        $application = new Application(static::kernel());
        $application->setAutoExit(false);
        $command = ['command' => 'lexik:jwt:generate-keypair --overwrite'];
        $application->run(new ArrayInput($command), new NullOutput());
        $this->serverUrl = $this->localServerUrl(Reader::readFromYamlFile(realpath('openapi.yml')));
        $this->authorization = [];
    }

    private function localServerUrl(OpenApi $openapi): string
    {
        return array_filter($openapi->servers, function (Server $server) {
            return $server->description === 'LOCAL';
        })[0]->url;
    }

    /**
     * @Given I have an item in database with identifier equals to :value
     */
    public abstract function iHaveAnItemInDatabaseWithIdentifierEqualsTo($value);

    protected abstract function tableName(): string;

    /**
     * @Given I am authenticated as a user
     */
    public function iAmAuthenticatedAsAUser()
    {
        $auth = $this->auth();
        $password = $this->passwordHasher->hashPassword($auth, $auth->getPassword());
        $user = $this->user($auth, $password);
        $this->userRepository->save($user);
        $this->authorization = ['HTTP_Authorization' => 'Bearer ' . $this->JWTTokenManager->create($auth)];
    }

    private function auth(): Auth
    {
        return new Auth(
            UserId::create(),
            EmailMother::random(),
            WordMother::password(),
            UserRoles::createFromPrimitives([UserRole::ROLE_USER->toString()])
        );
    }

    private function user(Auth $auth, string $password): User
    {
        return new User($auth->id(), new Email($auth->email()), $password, $auth->roles());
    }


    /**
     * @When I send a :method request to :path
     */
    public function iSendARequestTo($method, $path)
    {
        $this->request = Request::create($this->serverUrl . $path, $method, server: $this->authorization);

        $this->response = static::$kernel->handle($this->request);
    }

    /**
     * @When I send a :method request to :path with body:
     */
    public function iSendARequestToWithBody($method, $path, PyStringNode $body)
    {
        $parameters = $this->json($body->__toString());
        $this->request = Request::create($this->serverUrl . $path, $method, $parameters, server: $this->authorization);
        $this->response = static::$kernel->handle($this->request);
    }

    /**
     * @Then I should receive a response with status :code
     */
    public function iShouldReceiveAResponseWithStatus($code)
    {
        Assertions::assertEquals($code, $this->response->getStatusCode());
    }

    /**
     * @Then I should receive a response that conform to the specification
     */
    public function iShouldReceiveAResponseThatConformToTheSpecification()
    {
        Assertions::assertTrue(
            $this->validator()->validate(
                $this->response,
                $this->path(),
                $this->request->getMethod()
            )
        );
    }

    private function path(): string
    {
        $path = $this->router->getRouteCollection()->get(
            $this->request->attributes->get('_route')
        )->getPath();
        return preg_replace('/^\/api\/v\d+/', '', $path);
    }

    private function validator(): ValidatorInterface
    {
        return ValidatorBuilder::fromYaml('openapi.yml')
            ->getValidator();

    }

    /**
     * @Then I should receive a response with JSON nodes :keys
     */
    public function iShouldReceiveAResponseWithJsonNodes($keys)
    {
        $strippedKeys = preg_replace('/\s/', '', $keys);
        $explodedKeys = explode(',', $strippedKeys);
        $jsonResponse = $this->jsonResponse();
        foreach ($explodedKeys as $explodedKey) {
            Assertions::assertArrayHasKey($explodedKey, $jsonResponse);
        }
    }

    /**
     * @Then I should receive a response with value :value for JSON node :key
     */
    public function iShouldReceiveAResponseWithValueForJsonNode($value, $key)
    {
        $jsonResponse = $this->jsonResponse();
        Assertions::assertEquals($value, $jsonResponse[$key]);
    }

    /**
     * @BeforeSuite
     */
    public static function createDatabaseAndMigrations()
    {
        self::runDoctrineDatabase();
    }

    private static function kernel(): KernelInterface
    {
        return static::$kernel;
    }

    /**
     * @AfterScenario @database
     */
    public function cleanDatabase()
    {
        $connection = $this->entityManager->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeStatement($platform->getTruncateTableSQL($this->tableName(), true));
        $connection->executeStatement($platform->getTruncateTableSQL('users', true));
    }

    protected function jsonResponse(): mixed
    {
        return $this->json($this->response->getContent());
    }

    protected function json(string $data): mixed
    {
        return json_decode($data, true);
    }

}