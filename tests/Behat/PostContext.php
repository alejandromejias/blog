<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use App\Blog\Domain\Post\PostRepository;
use App\Blog\Domain\User\UserRepository;
use App\Tests\Unit\Domain\Post\PostIdMother;
use App\Tests\Unit\Domain\Post\PostMother;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;

class PostContext extends ApiRestContext
{
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        UserPasswordHasherInterface $passwordHasher,
        UserRepository $userRepository,
        JWTTokenManagerInterface $JWTTokenManager,
        private PostRepository $postRepository
    ) {
        parent::__construct($entityManager, $router, $passwordHasher, $userRepository, $JWTTokenManager);
    }

    public function iHaveAnItemInDatabaseWithIdentifierEqualsTo($value)
    {
        $post = PostMother::random(['id' => PostIdMother::create($value)]);
        $this->postRepository->save($post);
    }

    protected function tableName(): string
    {
        return 'posts';
    }
}